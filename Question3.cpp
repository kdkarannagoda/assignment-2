#include<stdio.h>
#include<conio.h>

int main(){
	char letter;
	int i=0,k=0;
	char vowel[]={'A','a','E','e','I','i','O','o','U','u'}; //vowel array
	printf("Enter your character :");
	scanf("%c",&letter); //to take a character from the user
	for(i;i<10;i++){
		if(letter==vowel[i]){ //to check the character is a vowel or not
			printf("Letter %c is a vowel.",letter); //prints vowel
			k=1; //if it is a vowel value of k = 1
			break; //if the condition is true, break the loop
		}
	}
	if(k==0){//if integer k is not equal to 1 it means character should be a consonant
		printf("Letter %c is a consonant.",letter);
	}	
	return 0;
}

